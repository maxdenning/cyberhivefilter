#include <fltKernel.h>
#include <dontuse.h>
#include <suppress.h>
#include <wchar.h>

// prototypes
static NTSTATUS UnloadFilterCallback(FLT_FILTER_UNLOAD_FLAGS flags);
static FLT_PREOP_CALLBACK_STATUS FLTAPI PreOperationRead(PFLT_CALLBACK_DATA data, PCFLT_RELATED_OBJECTS flt_objects, PVOID* context);

// file name search string
constexpr WCHAR* MATCH_STRING = L"cyberhive";

constexpr FLT_REGISTRATION filter_registration = {
	sizeof(FLT_REGISTRATION),
	FLT_REGISTRATION_VERSION,
	0,
	nullptr,
	filter_callbacks,
	UnloadFilterCallback,
	nullptr,
	nullptr,
	nullptr,
	nullptr,
	nullptr,
	nullptr,
	nullptr,
	nullptr
};

constexpr FLT_OPERATION_REGISTRATION filter_callbacks[] = {
	{
		IRP_MJ_READ,
		0,
		PreOperationRead,
		nullptr
	},
	{
		IRP_MJ_OPERATION_END
	}
};

PFLT_FILTER filter_handle = nullptr;


NTSTATUS DriverEntry(PDRIVER_OBJECT driver_object, PUNICODE_STRING registry_path)
{
	// register driver
	NTSTATUS status = FltRegisterFilter(driver_object, &filter_registration, &filter_handle);
	if (!NT_SUCCESS(status))
	{
		return status;
	}

	// start driver
	status = FltStartFiltering(filter_handle);
	if (!NT_SUCCESS(status))
	{
		// unregister if cannot start
		FltUnregisterFilter(filter_handle);
	}

	return status;
}

static NTSTATUS UnloadFilterCallback(FLT_FILTER_UNLOAD_FLAGS flags)
{
	// unregister if filter exists
	if (filter_handle != nullptr)
	{
		FltUnregisterFilter(filter_handle);
	}
	return STATUS_SUCCESS;
}

static FLT_PREOP_CALLBACK_STATUS FLTAPI PreOperationRead(PFLT_CALLBACK_DATA data, PCFLT_RELATED_OBJECTS flt_objects, PVOID* context)
{
	PFLT_FILE_NAME_INFORMATION file_name_information;
	NTSTATUS status = FltGetFileNameInformation(data, FLT_FILE_NAME_NORMALIZED | FLT_FILE_NAME_QUERY_DEFAULT, &file_name_information);
	if (!NT_SUCCESS(status))
	{
		return FLT_PREOP_SUCCESS_NO_CALLBACK;
	}

	// get file name
	status = FltParseFileNameInformation(file_name_information);
	if (NT_SUCCESS(status))
	{
		// search for MATCH_STRING and log if found
		WCHAR* match = wcsstr(file_name_information->Name.Buffer, MATCH_STRING);
		if (match != nullptr)
		{
			DbgPrint("File opened for read operation with name containing \"%wZ\": %wZ\n", MATCH_STRING, file_name_information->Name.Buffer);
		}
	}

	FltReleaseFileNameInformation(file_name_information);
	return FLT_PREOP_SUCCESS_NO_CALLBACK;
}
