# CyberHive File System Filter Driver
Uses a file system minifilter driver to recieve a callback whenever a file is read. Writes to the kernel debug log if that file has "cyberhive" in its path.

The code is untested as it is recommended that file system filter drivers are debugged remotely and I did not have access to a Windows virtual machine.

Additionally, much of the time I allotted for this task was used setting up the environment and reading introductory documentation as I have no experience with Windows driver development.


TODO:
* generate more robust output when a matching file name is found, e.g. show dialog box
* add preprocessor flags to only use UnloadFilterCallback in debug builds, as it is not recommended for release builds
* only match files with cyberhive in the file name, not in the full directory path
